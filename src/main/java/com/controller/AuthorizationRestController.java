package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorizationRestController {

    @GetMapping(value = "/api/v1/health")
    public String getHealth(){
        return "Get Health";
    }

    @GetMapping(value = "/api/v2/test")
    public String getTest(){
        return "Get Test";
    }
}
